GET http://localhost:8080/api/produkty?index=1

{
    "id": 1,
    "name": "Książka",
    "price": 5.11,
    "aviable": true
}

GET http://localhost:8080/api/produkty/all

[
    {
        "id": 1,
        "name": "Książka",
        "price": 5.11,
        "aviable": true
    },
    {
        "id": 2,
        "name": "Zeszyt56",
        "price": 2.33,
        "aviable": true
    },
    {
        "id": 3,
        "name": "Książka",
        "price": 35.0,
        "aviable": true
    },
    {
        "id": 4,
        "name": "Płyta Muzyczna",
        "price": 50.0,
        "aviable": true
    },
    {
        "id": 5,
        "name": "Dlugopis",
        "price": 2.0,
        "aviable": true
    },
    {
        "id": 6,
        "name": "Zeszyt123123",
        "price": 5.0,
        "aviable": false
    }
]
POST http://localhost:8080/api/produkty/

Body: 
{
        "id": 8,
        "name": "Książka123",
        "price": 6.66,
        "aviable": false
    }

Po operacji:

[
    {
        "id": 1,
        "name": "Książka",
        "price": 5.11,
        "aviable": true
    },
    {
        "id": 2,
        "name": "Zeszyt56",
        "price": 2.33,
        "aviable": true
    },
    {
        "id": 3,
        "name": "Książka",
        "price": 35.0,
        "aviable": true
    },
    {
        "id": 4,
        "name": "Płyta Muzyczna",
        "price": 50.0,
        "aviable": true
    },
    {
        "id": 5,
        "name": "Dlugopis",
        "price": 2.0,
        "aviable": true
    },
    {
        "id": 6,
        "name": "Zeszyt123123",
        "price": 5.0,
        "aviable": false
    },
    {
        "id": 7,
        "name": "Książka123",
        "price": 6.66,
        "aviable": false
    },
    {
        "id": 8,
        "name": "Książka123",
        "price": 6.66,
        "aviable": false
    }
]

PUT http://localhost:8080/api/produkty/

Body: 
{
        "id": 8,
        "name": "Książka123 ZMIENIONA",
        "price": 6.66,
        "aviable": false
    }
Po operacji:
[
    {
        "id": 1,
        "name": "Książka",
        "price": 5.11,
        "aviable": true
    },
    {
        "id": 2,
        "name": "Zeszyt56",
        "price": 2.33,
        "aviable": true
    },
    {
        "id": 3,
        "name": "Książka",
        "price": 35.0,
        "aviable": true
    },
    {
        "id": 4,
        "name": "Płyta Muzyczna",
        "price": 50.0,
        "aviable": true
    },
    {
        "id": 5,
        "name": "Dlugopis",
        "price": 2.0,
        "aviable": true
    },
    {
        "id": 6,
        "name": "Zeszyt123123",
        "price": 5.0,
        "aviable": false
    },
    {
        "id": 7,
        "name": "Książka123",
        "price": 6.66,
        "aviable": false
    },
    {
        "id": 8,
        "name": "Książka123 ZMIENIONA",
        "price": 6.66,
        "aviable": false
    }
]

PATCH http://localhost:8080/api/produkty/
Body:
{
        "id": 8,
        "name": "Książka123 ZMIENIONA PATCHEM",
        "price": 6.66,
        "aviable": false
    }

po operacji:
[
    {
        "id": 1,
        "name": "Książka",
        "price": 5.11,
        "aviable": true
    },
    {
        "id": 2,
        "name": "Zeszyt56",
        "price": 2.33,
        "aviable": true
    },
    {
        "id": 3,
        "name": "Książka",
        "price": 35.0,
        "aviable": true
    },
    {
        "id": 4,
        "name": "Płyta Muzyczna",
        "price": 50.0,
        "aviable": true
    },
    {
        "id": 5,
        "name": "Dlugopis",
        "price": 2.0,
        "aviable": true
    },
    {
        "id": 6,
        "name": "Zeszyt123123",
        "price": 5.0,
        "aviable": false
    },
    {
        "id": 7,
        "name": "Książka123 ZMIENIONA PATCHEM",
        "price": 6.66,
        "aviable": false
    }
]

GET http://localhost:8080/api/order/all

[
    {
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            },
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "wykonane"
    }
]

GET http://localhost:8080/api/order?index=1
{
    "id": 1,
    "customer": {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    "products": [
        {
            "id": 2,
            "name": "Zeszyt56",
            "price": 2.33,
            "aviable": true
        },
        {
            "id": 1,
            "name": "Książka",
            "price": 5.11,
            "aviable": true
        }
    ],
    "placeDate": "2021-06-19T03:45:23.977302",
    "status": "wykonane"
}

POST http://localhost:8080/api/order/

Body:

{
    "id": 2,
    "customer": {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    "products": [
        {
            "id": 2,
            "name": "Zeszyt56",
            "price": 2.33,
            "aviable": true
        },
        {
            "id": 1,
            "name": "Książka",
            "price": 5.11,
            "aviable": true
        }
    ],
    "placeDate": "2021-06-19T03:45:23.977302",
    "status": "wykonane"
}

Po operacji:

[
    {
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            },
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "wykonane"
    },
    {
        "id": 2,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            },
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "wykonane"
    }
]

PUT http://localhost:8080/api/order

Body:
{
    "id": 2,
    "customer": {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    "products": [
        {
            "id": 2,
            "name": "Zeszyt56",
            "price": 2.33,
            "aviable": true
        },
        {
            "id": 1,
            "name": "Książka",
            "price": 5.11,
            "aviable": true
        }
    ],
    "placeDate": "2021-06-19T03:45:23.977302",
    "status": "do wykonania PO PUT"
}

Po operacji:

[
    {
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            },
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "wykonane"
    },
    {
        "id": 2,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            },
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "do wykonania PO PUT"
    }
]

PATCH http://localhost:8080/api/order

Body:
{
    "id": 1,
    "customer": {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    "products": [
        {
            "id": 2,
            "name": "Zeszyt56",
            "price": 2.33,
            "aviable": true
        },
        {
            "id": 1,
            "name": "Książka",
            "price": 5.11,
            "aviable": true
        }
    ],
    "placeDate": "2021-06-19T03:45:23.977302",
    "status": "do wykonania PO PATCH"
}

Po operacji:

[
    {
        "id": 1,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            },
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "do wykonania PO PATCH"
    },
    {
        "id": 2,
        "customer": {
            "id": 1,
            "name": "Mirosław Duras",
            "adress": "Wrocław"
        },
        "products": [
            {
                "id": 1,
                "name": "Książka",
                "price": 5.11,
                "aviable": true
            },
            {
                "id": 2,
                "name": "Zeszyt56",
                "price": 2.33,
                "aviable": true
            }
        ],
        "placeDate": "2021-06-19T03:45:23.977302",
        "status": "do wykonania PO PUT"
    }
]

GET http://localhost:8080/api/customer/all
[
    {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    {
        "id": 2,
        "name": "Mirosław",
        "adress": "mirek30123@gmail.com"
    }
]


GET http://localhost:8080/api/customer?index=2

{
    "id": 2,
    "name": "Mirosław",
    "adress": "mirek30123@gmail.com"
}

POST http://localhost:8080/api/customer/

Body:

{
    "id": 3,
    "name": "Arkadiusz",
    "adress": "arek@gmail.com"
}

Po operacji:

[
    {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    {
        "id": 2,
        "name": "Mirosław",
        "adress": "mirek30123@gmail.com"
    },
    {
        "id": 3,
        "name": "Arkadiusz",
        "adress": "arek@gmail.com"
    }
]

PUT http://localhost:8080/api/customer/
Body:

{
    "id": 3,
    "name": "Arkadiusz PO PUT",
    "adress": "arek@gmail.com"
}

Po operacji:
[
    {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    {
        "id": 2,
        "name": "Mirosław",
        "adress": "mirek30123@gmail.com"
    },
    {
        "id": 3,
        "name": "Arkadiusz PO PUT",
        "adress": "arek@gmail.com"
    }
]

PATCH http://localhost:8080/api/customer/

Body:

{
    "id": 2,
    "name": "Arkadiusz PO PATCH",
    "adress": "arek@gmail.com"
}

Po operacji:

[
    {
        "id": 1,
        "name": "Mirosław Duras",
        "adress": "Wrocław"
    },
    {
        "id": 2,
        "name": "Arkadiusz PO PATCH",
        "adress": "arek@gmail.com"
    },
    {
        "id": 3,
        "name": "Arkadiusz PO PUT",
        "adress": "arek@gmail.com"
    }
]