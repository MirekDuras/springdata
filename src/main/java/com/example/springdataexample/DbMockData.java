package com.example.springdataexample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepository orderRepository;
    private CustomerRepository customerRepository;
    private UserRepository userRepository;
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepository orderRepository, CustomerRepository customerRepository, UserRepository userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository=userRepository;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Książka", 5.11f, true);
        Product product1 = new Product("Zeszyt56", 2.33f, true);
        Customer customer = new Customer("Mirosław Duras", "Wrocław");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "wykonane");
        String password="user123";
        String password2="admin123";
        UserDto userDto = new UserDto("mirek","mirek","CUSTOMER");
        UserDto uzytkownikCustomer = new UserDto("user",passwordEncoder.encode(password),"ROLE_CUSTOMER");
        UserDto uzytkownikAdmin = new UserDto("admin",passwordEncoder.encode(password2),"ROLE_ADMIN");
        User user2=new User("mirek","mirek","CUSTOMER");

       // User user3 = userDtoBuilder.user(userDto);

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        orderRepository.save(order);
        //userRepository.save(userDto);
        userRepository.save(uzytkownikAdmin);
        userRepository.save(uzytkownikCustomer);

    }
}