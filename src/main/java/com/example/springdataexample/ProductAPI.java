package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductAPI {

    private ProductManager produkty;


    @Autowired
    public ProductAPI(ProductManager produkty) {
        this.produkty = produkty;
    }


// private List<Product> produkty;
/*
    public ProductAPI() {
        produkty= new ArrayList<>();
        produkty.add(new Product(Long.parseLong("11"), "siema",12,true));
        produkty.add(new Product(Long.parseLong("12"), "siema2",123,true));
        produkty.add(new Product(Long.parseLong("13"), "siema3",123,true));
        produkty.add(new Product(Long.parseLong("14"), "siema4",123,true));
    }

 */

    @GetMapping("/produkty/all")
    public Iterable<Product> getAll(){
        return produkty.findAll();

    }

    @GetMapping("/produkty")
    public Optional<Product> getById(@RequestParam Long index){
        return produkty.findById(index);
    }

    @PostMapping("admin/produkty")
    public Product addProduct (@RequestBody Product produkt){
        return produkty.save(produkt);
    }

    @PutMapping("admin/produkty")
    public Product updateProduct (@RequestBody Product produkt){
        return produkty.save(produkt);
    }


    @PatchMapping("admin/produkty")
    public Product patchProduct (@RequestBody Map<String,Object> updates, @PathVariable Long id){
        Product product = produkty.savePatch(updates, id);
        if(product ==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"nie znaleziono");
        return product;
    }
/*
    @DeleteMapping("admin/produkty2")
    public void deleteProduct (@RequestParam Long index){
        produkty.deleteById(index);


    }

 */





}
