package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class CustomerManager {

    private CustomerRepository customerRepository;

    @Autowired
    public void CustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> findById(Long id){
        return customerRepository.findById(id);
    }


    public Iterable<Customer> findAll(){
        return customerRepository.findAll();
    }

    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    public Customer savePatch(Map<String,Object> updates, Long id){
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if(!customerOptional.isPresent())
            return null;

        Customer customer  = customerOptional.get();
        if(updates.containsKey("name")) customer.setName((String) updates.get("name"));
        if(updates.containsKey("adress")) customer.setAdress(((String) updates.get("adress")));


        return customerRepository.save(customer);
    }


    public void deleteById(Long id){
        customerRepository.deleteById(id);
    }


}