package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderAPI {

    private OrderManager orders;


    @Autowired
    public OrderAPI(OrderManager orders) {
        this.orders = orders;
    }


// private List<Product> produkty;
/*
    public ProductAPI() {
        produkty= new ArrayList<>();
        produkty.add(new Product(Long.parseLong("11"), "siema",12,true));
        produkty.add(new Product(Long.parseLong("12"), "siema2",123,true));
        produkty.add(new Product(Long.parseLong("13"), "siema3",123,true));
        produkty.add(new Product(Long.parseLong("14"), "siema4",123,true));
    }

 */

    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orders.findAll();

    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam Long index){
        return orders.findById(index);
    }

    @PostMapping("admin/order")
    public Order addProduct (@RequestBody Order order){
        return orders.save(order);
    }

    @PutMapping("admin/order")
    public Order updateProduct (@RequestBody Order order){
        return orders.save(order);
    }
//{id}
    @PatchMapping("admin/order")
    public Order patchProduct (@RequestBody Map<String,Object> updates, @PathVariable Long id){
        Order order = orders.savePatch(updates, id);
        if(order ==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"nie znaleziono");
        return order;
    }
/*
    @DeleteMapping("admin/order2")
    public void deleteProduct (@RequestParam Long index){
        orders.deleteById(index);
    }

 */





}