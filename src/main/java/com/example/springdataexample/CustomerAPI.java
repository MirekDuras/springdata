package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerAPI {

    private CustomerManager customers;


    @Autowired
    public CustomerAPI(CustomerManager customers) {
        this.customers = customers;
    }


// private List<Product> produkty;
/*
    public ProductAPI() {
        produkty= new ArrayList<>();
        produkty.add(new Product(Long.parseLong("11"), "siema",12,true));
        produkty.add(new Product(Long.parseLong("12"), "siema2",123,true));
        produkty.add(new Product(Long.parseLong("13"), "siema3",123,true));
        produkty.add(new Product(Long.parseLong("14"), "siema4",123,true));
    }

 */

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customers.findAll();

    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long index){
        return customers.findById(index);
    }

    @PostMapping("admin/customer")
    public Customer addProduct (@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PutMapping("admin/customer")
    public Customer updateProduct (@RequestBody Customer customer){
        return customers.save(customer);
    }


    @PatchMapping("admin/customer")
    public Customer patchProduct (@RequestBody Map<String,Object> updates, @PathVariable Long id){

        Customer customer = customers.savePatch(updates, id);
        if(customer ==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"nie znaleziono");
        return customer;

    }


    /*
    @DeleteMapping("admin/customer2")
    public void deleteProduct (@RequestParam Long index){
        customers.deleteById(index);
    }

     */





}