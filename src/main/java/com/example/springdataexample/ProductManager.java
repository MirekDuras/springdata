package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductManager {

    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Optional<Product> findById(Long id){
        return productRepo.findById(id);
    }


    public Iterable<Product> findAll(){
        return productRepo.findAll();
    }

    public Product save(Product product){
        return productRepo.save(product);
    }

    public Product savePatch(Map<String,Object> updates, Long id){
        Optional<Product> productOptional = productRepo.findById(id);
        if(!productOptional.isPresent())
            return null;

        Product product = productOptional.get();
        if(updates.containsKey("name")) product.setName((String) updates.get("name"));
        if(updates.containsKey("price")) product.setPrice(((Double) updates.get("price")).floatValue());
        if(updates.containsKey("aviable")) product.setAviable((Boolean) updates.get("aviable"));

        return productRepo.save(product);
    }


    public void deleteById(Long id){
        productRepo.deleteById(id);
    }


}
