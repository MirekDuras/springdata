package com.example.springdataexample;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {


    private ProductRepo productRepo;
    private CustomerRepository customerRepository;
    private UserRepository userRepository;

    @Autowired
    public Start(ProductRepo productRepo, CustomerRepository customerRepository, UserRepository userRepository) {
        this.productRepo = productRepo;
        this.customerRepository=customerRepository;
        this.userRepository=userRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void runExample() {
        Product product1 = new Product("Książka",35 ,true);
        Product product2 = new Product("Płyta Muzyczna",50 ,true);
        Product product3 = new Product("Dlugopis",2 ,true);
        Product product4 = new Product("Zeszyt123123",5,false);
        Customer customer1 = new Customer("Mirosław","mirek30123@gmail.com");
        UserDto userDto = new UserDto("mirek","mirek","CUSTOMER");
        productRepo.save(product1);
        productRepo.save(product2);
        productRepo.save(product3);
        productRepo.save(product4);
        customerRepository.save(customer1);
        userRepository.save(userDto);
    }
}
