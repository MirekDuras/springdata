package com.example.springdataexample;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;

    private String name;
    private float price;
    private boolean aviable;

    public Product() {
    }

    public Product(String name, float price, boolean aviable) {
        this.name = name;
        this.price = price;
        this.aviable = aviable;
    }

    public Product(Long id, String name, float price, boolean aviable) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.aviable = aviable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAviable() {
        return aviable;
    }

    public void setAviable(boolean aviable) {
        this.aviable = aviable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", aviable=" + aviable +
                '}';
    }
}
