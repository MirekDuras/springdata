package com.example.springdataexample;

public class UserDtoBuilder {

    public UserDto userDto (User user){
        UserDto newUserDto = new UserDto(user.getName(),user.getPassword());
        return newUserDto;
    }

    public UserDto userDtoWithRole (User user){
        UserDto newUserDto = new UserDto(user.getName(),user.getPassword(), user.getRole());
        return newUserDto;
    }

    public User user(UserDto userDto){
        User user = new User(userDto.getName(),userDto.getPasswordHash(),userDto.getRole());
        return user;
    }
    public User user2(UserDto userDto){
        User user = new User(userDto.getName(),userDto.getPasswordHash());
        return user;
    }

}
