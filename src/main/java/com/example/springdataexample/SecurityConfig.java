package com.example.springdataexample;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    @Autowired
    private DataSource dataSource;



    @Autowired
    UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name="passwordEncoder")
    public PasswordEncoder passwordencoder(){
        return new BCryptPasswordEncoder();
    }






    @Override
    public void configure(WebSecurity web){
        web.ignoring().antMatchers("/h2/**");
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());;

    }

/*
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                    .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM userdto u WHERE u.name=?")
                    .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM userdto u WHERE u.name=?")
                    .dataSource(dataSourceConfig.getDataSource())
                     .passwordEncoder(passwordEncoder());;

          .dataSource()
                .withDefaultSchema()
                .withUser(User.withUsername("user")
                        .password(passwordEncoder().encode("pass"))
                        .roles("USER"));



        .dataSource(dataSource)
                .withDefaultSchema()
                .withUser(User.withUsername("user")
                        .password(passwordEncoder().encode("pass"))
                        .roles("USER"));
                .usersByUsernameQuery("SELECT u.name, u.password_Hash, 1 FROM UserDto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM UserDto u WHERE u.name=?")
                .dataSource(dataSource);


    }

 */


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers(HttpMethod.GET,"/api/produkty/all").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/produkty").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/order/all").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/order").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.POST,"/api/admin/order").hasAnyRole("CUSTOMER","ADMIN")


                .antMatchers(HttpMethod.GET,"/api/customer/all").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/customer").hasRole("CUSTOMER")


                .antMatchers(HttpMethod.POST,"/api/admin/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"admin/produkty").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"admin/produkty").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"admin/produkty").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"admin/order").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"admin/order").hasRole("ADMIN")

                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll()
                .and()
                .httpBasic().and().csrf().disable();
    }




}
