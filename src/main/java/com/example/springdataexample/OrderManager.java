package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager {

    private OrderRepository orderRepository;

    @Autowired
    public void OrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Optional<Order> findById(Long id){
        return orderRepository.findById(id);
    }


    public Iterable<Order> findAll(){
        return orderRepository.findAll();
    }

    public Order save(Order order){
        return orderRepository.save(order);
    }

    public Order savePatch(Map<String,Object> updates, Long id){
        Optional<Order> orderOptional = orderRepository.findById(id);
        if(!orderOptional.isPresent())
            return null;

        Order order  = orderOptional.get();
        if(updates.containsKey("status")) order.setStatus((String) updates.get("status"));
        if(updates.containsKey("placeDate")) order.setPlaceDate(((LocalDateTime) updates.get("placeDate")));
       // if(updates.containsKey("products")) order.setProducts((Set<Product>) updates.get("products"));
        if(updates.containsKey("customer")) order.setCustomer((Customer) updates.get("customer"));

        return orderRepository.save(order);
    }

    public void deleteById(Long id){
        orderRepository.deleteById(id);
    }


}