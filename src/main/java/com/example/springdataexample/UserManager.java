package com.example.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManager {

    private com.example.springdataexample.UserRepository userRepository;

    @Autowired
    public void UserRepository(com.example.springdataexample.UserRepository userRepository) {
        this.userRepository = userRepository;
    }



    /*
    public User save(User user){
        return userRepository.save(user);
    }

     */



    public com.example.demo.potrzbne.UserDto save(com.example.demo.potrzbne.UserDto user){
        return userRepository.save(user);
    }
/*
    public Optional<User> findById(Long id){
        return userRepository.findById(id);
    }

 */





}